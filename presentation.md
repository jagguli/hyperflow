#Hyperflow with Python Qtile


---

#But first ...

---

## What is flow


> Flow is the mental state of operation in which
> a person performing an activity is fully
> immersed in a feeling of energized focus, full
> involvement, and enjoyment in the process of
> the activity.
> -- <cite>[Wikipedia][1]</cite>


<br>

![typigngeek](images/typinggeek.gif)


<!---
> According to Csikszentmihalyi, flow is completely focused
> motivation. It is a single-minded immersion and represents perhaps the
> ultimate experience in harnessing the emotions in the service of
> performing and learning. In flow, the emotions are not just contained
> and channeled, but positive, energized, and aligned with the task at
> hand. The hallmark of flow is a feeling of spontaneous joy, even
> rapture, while performing a task[2] although flow is also described
> (below) as a deep focus on nothing but the activity – not even oneself
> or one's emotions.
> -- <cite>[Wikipedia][1]</cite>

-->

  #samadhi #trance #psychology #mindfullness

---

## What is flow


> Developers of computer software reference getting into a flow state
> as "wired in", or sometimes as The Zone,[38][39] hack mode,[40] or
> operating on software time[41] when developing in an undistracted
> state.
> -- <cite>[Wikipedia][1]</cite>


<br>

![devflow](images/How+we+imagine+programmers+work_73816a_4993240.gif)

  #cyberpunk

---

## What is flow


- Flow is one of the main reasons that people play video games.

![flow](images/flow_states.png)

  #gaming #rocketjump #hurricanekick

---

# Muscle memory

  
> When a movement is repeated over time, a long-term muscle memory is
> created for that task, eventually allowing it to be performed without
> conscious effort. This process decreases the need for attention and
> creates maximum efficiency within the motor and memory systems.
> -- <cite>[Wikipedia][2]</cite>

![musclememory](images/muscle-memory.jpg)

  #biohack #vim #emacs

[1]:http://en.wikipedia.org/wiki/Flow_(psychology)
[2]:http://en.wikipedia.org/wiki/Muscle_memory
---

# What breaks your flow.

- Distractions
- Repetitive tasks
- Inefficient work flow.
- reaching/searching for the mouse 

<br>

![mousehand](images/noside.jpg)


<!---
    ![kernelcat](images/IMG_2893r.JPG)

    ---

    # What breaks your flow.
    ![genius](images/kernel_panic.jpg)

-->


---

# What breaks your flow.

![toomanywindows](images/too-many-folders-on-desktop.jpg)

---

# What breaks your flow.

![scumbag](images/scumbag-windows-strikes-again.jpg)

---

<video controls width="100%" height="100%" src="videos/obama.mp4#t=23,29"></video>

c'mon guys .. breakin my flow all the time

---

# What is hyperflow.

    #troll

 - thats just something I made up.

 ![troll](images/Famous-characters-Troll-face-Troll-face-poker-45046.png =300x)

 
 - could mean awesome flow :)
 - super flow just don't sound right ...
 - awesome flow might have been better ... but its ambiguous ... you'll see
 - but there is a concept related to flow called hyperfocus ...

 ... anyways

---

# Tiling Window Managers

---

#Why
> I've been looking into tiling window managers
> because I'm tired of manually positioning and
> resizing all my windows whenever I want to see
> some information side by side.
> -- <cite>Kyle Cronin on [StackOverflow][2]</cite>

[2]:http://superuser.com/questions/13395/tiling-window-manager

![alttabhell](images/alttabhell.jpg)


---
 
# Tiling Window Managers
> I've been hearing a lot about tiling window managers lately. People
> seem to love them, swear by them, even wonder why everyone doesn't use
> them. But after searching for a little bit, I couldn't find anyone
> explaining why they make any sense at all.
> -- <cite>itsadok on [StackOverflow][3]</cite>

![yoda](images/luke-yoda.jpg)

> You won't get the answer to your question by
> searching. You won't even understand it even
> after asking here and reading the answers :-)
> You have to try it for yourself and see the
> bright sides.
> -- <cite>vtest on [StackOverflow][3]</cite>

[3]:http://superuser.com/questions/13395/tiling-window-manager

---

# Tiling Window Managers

![enlighted](images/enlightenment.gif)

> It's quite possible that anyone swearing over a window tilling usage
> pattern may be part of an "enlightened elite"; Those who were able
> to overcome deeply ingrained computer usage habits that we tend to
> overlook. I'm pretty sure they don't support it on every case, but
> swear by it on many cases simply because when we really look at our
> screens, we get the shocking revelation that for many common tasks
> we have been very wasteful.
> -- <cite>[Wikipedia][5]</cite>

---

> In computing, a tiling window manager is a window manager with an
> organization of the screen into mutually non-overlapping frames, as
> opposed to the more popular approach of coordinate-based stacking of
> overlapping objects (windows) that tries to fully emulate the desktop
> metaphor.
> -- <cite>[Wikipedia][5]</cite>

<br>

- Been around since 1982 <sup>[5]</sup>
- Most window managers have tiling functionality (including win95 onward)

---

# Popular tiling window managers.

![dwm](images/Dwm-logo.png)
dwm - minimal c source, config is source, fast, suckless


![awesome](images/Awesome_logo.png)
awesome - derived from dwm, configured in lua.


![xmonad](images/64px-Xmonad-logo.svg.png)
xmonad - “That was easy. xmonad rocks!” ... written in haskell, influenced by dwm

stumpwm - "Like Terminator but with frames." ... common lisp, very nice ... 


![rat](images/64px-Ratpoison_new.png)
ratpoison - "say good bye to the rodent."

... i3, wmii, Musca, Ion ... and many more .

[5]:http://en.wikipedia.org/wiki/Tiling_window_manager

---

![frywm](images/JBYFSxa.png)

---

# Introducing Qtile

---

# Introducing Qtile


- A full-featured, hackable tiling window manager written in Python <sup>[4]</sup>.
- Qtile is simple, small, and extensible. It's easy to write your own layouts, widgets, and built-in commands.
- Qtile is written and configured entirely in Python, which means you can leverage the full power and flexibility of the language to make it fit your needs.
- The Qtile community is active and growing, so there's always someone to lend a hand when you need help.
- Qtile is free and open-source software, distributed under the permissive MIT license.

> http://qtile.org/

  #python #hackable #active #support

---

# Key Concepts

---

## Groups


- A group is a container for a bunch of windows, analogous to workspaces in other window managers.
- Each client window managed by the window manager belongs to exactly one group.

![grouping](images/grouping-11.png)

  #organisation

---

<video controls width="100%" height="100%" src="videos/groups.ogv"></video>

---

## Layouts



- A layout is an algorithm for laying out windows in a group on your screen.
- Since Qtile is a tiling window manager, this usually means that we try to use space as efficiently as possible, and give the user ample commands that can be bound to keys to interact with layouts.

![foodlayout](images/food_layout_by_laszlo_616.jpg)

  #arrangement

---

<video controls width="100%" height="100%" src="videos/layouts.ogv"></video>

---

## Screens
- Defines the physical screens you want to use, and the bars and widgets associated with them.
- Most of the visible “look and feel” configuration will happen in this section.

![screens](images/matrix_026.jpg)

---

## Keys
- At a minimum, this will probably include bindings to switch between windows, groups and layouts.


## Mouse
- yes you can use the rodent. You must like CTS pain.


![mouse](images/wireless-mouse.png)

---

## Install and startup

### Install (Ubuntu)

    !bash
    sudo apt-add-repository ppa:tycho-s/ppa
    sudo apt-get update
    sudo apt-get install qtile


### Install (Arch)

    yaourt qtile-git
    
### Install (Source/Other)


![](images/keep-calm-and-rtfm.png)

<http://docs.qtile.org/en/latest/index.html>

  
---

### Starting

#### Starting in new X instance.

<https://github.com/qtile/qtile-examples>

    !bash
    cat qtile-xsession
    #!/bin/sh

    QTILE=$HOME/.qtile

    # make the cursor move the right speed
    xset mouse 2 9

    # impersonate LG3D so java swing apps work
    wmname LG3D

    exec qtile -c $QTILE/config.py

<br>

    !bash
    startx qtile-xsession -- :1


#### As default X session

- add to `.xinitrc` and `startx`

---

## Configuration


Qtile looks in the following places for a configuration file, in order:

1. The location specified by the -f argument.
2. $XDG_CONFIG_HOME/qtile/config.py, if it is set
3. ~/.config/qtile/config.py

It reads the module libqtile.resources.default_config, included by default with every qtile installation.

---

### Screens

- The screens variable contains information about what bars are drawn where on each screen.
- If you have multiple screens, you'll need to construct multiple Screen objects, each with whatever widgets you want.
- Below is a screen with a top bar that  contains several basic qtile widgets.

        !python
        screens = [Screen(top = bar.Bar([
                # This is a list of our virtual desktops.
                widget.GroupBox(urgent_alert_method='text'),

                # A prompt for spawning processes or switching groups.
                #This will be invisible most of the time.
                widget.Prompt(),

                # Current window name.
                widget.WindowName(),
                widget.Volume(),
                widget.Battery(
                    energy_now_file='charge_now',
                    energy_full_file='charge_full',
                    power_now_file='current_now',
                ),
                widget.Systray(),
                widget.Clock('%Y-%m-%d %a %I:%M %p'),
            ], 30)) # our bar is 30px high
        ]

---

### Keys

Super_L (the Windows key) is typically bound to mod4 by default, so we use that here.

    !python
    mod = "mod4"

The keys variable contains a list of all of the keybindings that qtile will look through each time there is a key pressed.

    !python
    keys = [
        Key([mod], "k",              lazy.layout.down()),
        Key([mod], "j",              lazy.layout.up()),
        Key([mod], "h",              lazy.layout.previous()),
        Key([mod], "l",              lazy.layout.previous()),
        Key([mod, "shift"], "space", lazy.layout.rotate()),
        .
        .
        .
        Key(
            [], "XF86AudioMute",
            lazy.spawn("amixer -c 0 -q set Master toggle")
        ),

        # Also allow changing volume the old fashioned way.
        Key([mod], "equal", lazy.spawn("amixer -c 0 -q set Master 2dB+")),
        Key([mod], "minus", lazy.spawn("amixer -c 0 -q set Master 2dB-")),
    ]

---

### Mouse


This allows you to drag windows around with the mouse if you want.

    !python
    mouse = [
        Drag([mod], "Button1", lazy.window.set_position_floating(),
            start=lazy.window.get_position()),
        Drag([mod], "Button3", lazy.window.set_size_floating(),
            start=lazy.window.get_size()),
        Click([mod], "Button2", lazy.window.bring_to_front())
    ]

![deadmouse](images/business-cat-meme-dead-mouse.jpg)

---

### Groups

Next, we specify group names, and use the group name list to generate an appropriate set of bindings for group switching.

    !python
    groups = []
    for i in ["a", "s", "d", "f", "u", "i", "o", "p"]:
        groups.append(Group(i))
        keys.append(
            Key([mod], i, lazy.group[i].toscreen())
        )
        keys.append(
            Key([mod, "mod1"], i, lazy.window.togroup(i))
        )

---

### Dynamic Groups

Dynamic groups and matching

    !python
    # dgroup rules that not belongs to any group
    dgroups_app_rules.extend([
        # Everything i want to be float, but don't want to change group
        Rule(Match(title=['nested', 'gscreenshot'],
                   wm_class=[
                    'Guake.py', 'Exe', 'Onboard', 'Florence',
                    'Terminal', 'Gpaint', 'Kolourpaint', 'Wrapper',
                    'Gcr-prompter', 'Ghost',
                    re.compile('Gnome-keyring-prompt.*?')],
                   ),
             float=True, intrusive=True),

        # floating windows
        Rule(Match(wm_class=["Pavucontrol", 'Wine', 'Xephyr', "Gmrun"]),
             float=True),
        Rule(Match(title=[".*Hangouts.*"]), group="6"),
        Rule(Match(wm_class=["Kmail"]), group="4"),
        Rule(Match(wm_class=["Pidgin"]), group="3", float=False),
        Rule(Match(wm_class=["KeePass2"]), float=True),
        Rule(Match(wm_class=["Kruler"]), float=True),
        Rule(Match(wm_class=["Screenkey"]), float=True, intrusive=True),
        Rule(Match(wm_class=["rdesktop"]), group="14"),
        Rule(Match(wm_class=[".*VirtualBox.*"]), group="13"),
    ])

---

### Layouts
Two basic layouts.

    !python
    layouts = [
        layout.Stack(stacks=2, border_width=1),
        layout.Max(),
    ]

More intersting layouts (customized)

    !python

    layouts = [
        layout.Max(),
        layout.Stack(),
        layout.xmonad.MonadTall(ratio=0.50),
        layout.Tile(),
        layout.Zoomy(),
        layout.TreeTab(),
        # a layout just for gimp
        layout.Slice('left', 192, name='gimp', role='gimp-toolbox',
                     fallback=layout.Slice('right', 256, role='gimp-dock',
                     fallback=layout.Stack(
                     num_stacks=1, **border_args))),
    ]

---


# Hooks

![hooks](images/hooks2.jpg)

<br>

Qtile provides a mechanism for subscribing to certain events in libqtile.hook.

To subscribe to a hook in your configuration, simply decorate a function with the hook you wish to subscribe to.

---

# Extending

- custom layouts
- custom placement/screen rules
- window matching
- complex keyboard commands for workflow
- test suite using xephyr (nested X server).
- it's python ... use your imagination.

<http://docs.qtile.org/en/latest/manual/hacking.html>

---

# Cool addons
- st (xterm replacement <http://suckless.org>)
- tmux (terminal multiplexer)
- dzen2 (dmenu)
- percol


<br>

![likeaboss](images/likeaboss.gif)

---

#The Qtile Community


- IRC: \#qtile on the OFTC network
- Google Groups: qtile-dev
- Twitter: @qtile

- Github <https://github.com/qtile/qtile>  <https://github.com/qtile/qtile-examples>

<video style="float" controls width="100%" loop src="videos/out.ogv"></video>

